module Consts where

-- | Messages used for bot interactio.
namePrompt, goodbyeMsg, noTasksMsg, completeMsg, newTaskMsg, botPrompt :: String
namePrompt = "Enter your name: "
goodbyeMsg = "Goodbye!"
noTasksMsg = "Bot> "
completeMsg = "Task completed!"
newTaskMsg = "New task added!"
botPrompt = "Bot> "
